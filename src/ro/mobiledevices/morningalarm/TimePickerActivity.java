package ro.mobiledevices.morningalarm;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import java.util.Calendar;

public class TimePickerActivity extends Activity {

    private DatePicker datePicker;
    private TimePicker timePicker;
    private RadioGroup radioGroup;
    private RadioButton setRadioButton;
    private RadioButton unsetRadioButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_layout);

        datePicker = (DatePicker) findViewById(R.id.datePicker);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        setRadioButton = (RadioButton) findViewById(R.id.set_radio_button);
        unsetRadioButton = (RadioButton) findViewById(R.id.unset_radio_button);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);

        datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.i("TimePickerActivity", "Date changed to "
                        + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth + "-" + " !");
            }
        });

        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minute);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                Log.i("TimePickerActivity", "Time changed to "
                        + hourOfDay + ":" + minute + " !");
            }
        });
    }

}
