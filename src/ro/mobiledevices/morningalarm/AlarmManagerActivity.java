package ro.mobiledevices.morningalarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AlarmManagerActivity extends Activity {

    private Button startButton1, startButton2, stopButton;
    private Button button3, button4, button5, button6;
    private EditText textSeconds;
    private Toast toast;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

//        ListViewLoader lvl = new ListViewLoader();

        startButton1 = (Button) findViewById(R.id.btnSetAlarm1);
        startButton2 = (Button) findViewById(R.id.btnSetAlarm2);
        stopButton = (Button) findViewById(R.id.btnStopAlarm);
        textSeconds = (EditText) findViewById(R.id.txtSeconds);

        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);

        startButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked set button 1!");
                try {
                    int i = Integer.parseInt(textSeconds.getText().toString());
                    Intent intent = new Intent(AlarmManagerActivity.this, AlarmReceiverActivity.class);
                    PendingIntent pendingIntent =
                            PendingIntent.getActivity(AlarmManagerActivity.this, 2, intent,
                                    PendingIntent.FLAG_CANCEL_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (i * 1000),
                            pendingIntent);
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(getApplicationContext(),
                            "Alarm for activity is set in " + i + " seconds!",
                            Toast.LENGTH_LONG);
                    toast.show();
                } catch (NumberFormatException e) {
                    if (toast != null) {
                        toast.cancel();
                    }
                    // A toast is a view containing a quick little message for the user
                    toast = Toast.makeText(AlarmManagerActivity.this,
                            "Please enter some number in the text field and try again!",
                            Toast.LENGTH_LONG);
                    toast.show();
                    Log.i("AlarmManagerActivity", "Number format exception");
                }
            }
        });

        startButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked set button 2!");
                try {
                    int i = Integer.parseInt(textSeconds.getText().toString());
                    Intent intent = new Intent(AlarmManagerActivity.this, RepeatingAlarmReceiverActivity.class);
                    PendingIntent pendingIntent =
                            PendingIntent.getActivity(AlarmManagerActivity.this, 3, intent,
                                    PendingIntent.FLAG_CANCEL_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (i * 1000),
                            15 * 1000, pendingIntent);
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(getApplicationContext(),
                            "Repeating alarm for activity is set in " + i + " seconds!",
                            Toast.LENGTH_LONG);
                    toast.show();
                } catch (NumberFormatException e) {
                    if (toast != null) {
                        toast.cancel();
                    }
                    // A toast is a view containing a quick little message for the user
                    toast = Toast.makeText(AlarmManagerActivity.this,
                            "Please enter some number in the text field and try again!",
                            Toast.LENGTH_LONG);
                    toast.show();
                    Log.i("AlarmManagerActivity", "Number format exception");
                }
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked stop button 1!");

                Intent intent = new Intent(AlarmManagerActivity.this, RepeatingAlarmReceiverActivity.class);
                PendingIntent pendingIntent =
                        PendingIntent.getActivity(AlarmManagerActivity.this, 3, intent, 0);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                if (toast != null) {
                    toast.cancel();
                }
                toast = Toast.makeText(getApplicationContext(),
                        "Repeating alarm for activity was canceled!",
                        Toast.LENGTH_LONG);
                toast.show();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked button 3!");
                Intent intentMain = new Intent(AlarmManagerActivity.this,
                        ListViewLoader.class);
                AlarmManagerActivity.this.startActivity(intentMain);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked button 4!");
                Intent intentMain = new Intent(AlarmManagerActivity.this,
                        ListViewExample.class);
                AlarmManagerActivity.this.startActivity(intentMain);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked button 5!");
                Intent intentMain = new Intent(AlarmManagerActivity.this,
                        ListActivityExample.class);
                AlarmManagerActivity.this.startActivity(intentMain);
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("AlarmManagerActivity", "Clicked button 6!");
                Intent intentMain = new Intent(AlarmManagerActivity.this,
                        TimePickerActivity.class);
                AlarmManagerActivity.this.startActivity(intentMain);
            }
        });
    }
}
