package ro.mobiledevices.morningalarm;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import java.io.IOException;

public class AlarmReceiverActivity extends Activity {

    private MediaPlayer mediaPlayer;
    // A wake lock is a mechanism to indicate that your application needs to have the device stay on.
    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the power manager from the system service
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        // Create the wake lock
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Wake Lock");
        // Acquire the lock - take the full control of the power system - until release
        wakeLock.acquire();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        );
        setContentView(R.layout.alarm);

        Button stopAlarm = (Button) findViewById(R.id.btnStopAlarm);
        stopAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
//                ringtone.stop();
                finish();
            }
        });

//        playSound(this, getAlarmUri());

        // Replace not working playSound()
        mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
        Thread background = new Thread(new Runnable() {
            public void run() {
                try {

                    mediaPlayer.start();

                } catch (Throwable t) {
                    Log.i("AlarmReceiverActivity", "Thread  exception " + t);
                }
            }
        });
        background.start();
    }

    private void playSound(Context context, Uri alert) {
        mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager =
                    (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IOException e) {
            Log.i("AlarmReceiver", "No audio files were found");
        }
    }

    private Uri getAlarmUri() {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Release the wake lock
        wakeLock.release();
    }
}
